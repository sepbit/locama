# Changelog

See [Keep a Changelog](http://keepachangelog.com/).

## [1.0.0] - 2021-11-28
### Added
- First release!
