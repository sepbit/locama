"""
Locama - Loterias da Caixa para Mastodon
Copyright (C) 2021  Vitor Guia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import re
import json
from time import time
from urllib.request import Request, urlopen

def mask_money(value):
    '''
    Money format
    '''
    value = '{:,.2f}'.format(float(value))
    value = str(value.replace('.', '_'))
    value = str(value.replace(',', '.'))
    return str(value.replace('_', ','))


def get_api(tipo_jogo):
    request = Request(
      'http://loterias.caixa.gov.br/wps/portal/loterias/landing/' + tipo_jogo + '/?' + str(time()),
      headers={
        'User-Agent': 'Mozilla/5.0',
        'Cookie': 'security=true'
      }
    )

    with urlopen(request) as response:
        response = response.read().decode('utf-8')

    base = re.findall('<base href="(.*)">', response)

    if base:
        base = base[0]
    else:
        return 'Base not found'

    urlBuscarResultado = re.findall('<input type="hidden" value="(.*)" ng-model="urlBuscarResultado" id="urlBuscarResultado" \/>', response)

    if urlBuscarResultado:
        urlBuscarResultado = urlBuscarResultado[0]
    else:
        return 'urlBuscarResultado not found'

    return base + urlBuscarResultado


def resultado(tipo_jogo):
    request = Request(
      get_api(tipo_jogo),
      headers={
        'User-Agent': 'Mozilla/5.0',
        'Cookie': 'security=true'
      }
    )

    with urlopen(request) as response:
        response = response.read()

    data = json.loads(response)

    tipo_jogo = data['tipoJogo'].replace('_', ' ')
    tipo_jogo = tipo_jogo.title()
    tipo_jogo = tipo_jogo.replace(' ', '')

    message = 'Jogo: #' + tipo_jogo
    message += '\nNúmero: ' + str(data['numero'])
    message += '\nData: ' + str(data['dataApuracao'])
    message += '\nDezenas: ' + ' '.join(data['listaDezenas'])

    if data['acumulado'] == True:
        message += '\nAcumulado R$:' + mask_money(data['valorEstimadoProximoConcurso'])
    else:
        message += '\nNão acumulou!'

    return message
