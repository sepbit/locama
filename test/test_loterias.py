"""
Locama - Loterias da Caixa para Mastodon
Copyright (C) 2021  Vitor Guia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import unittest
from sepbit.locama.loterias import mask_money, resultado


class LoteriasTest(unittest.TestCase):
    """
    Test loterias module
    """

    def test_mask_money(self):
        """
        Test mask_money function
        """
        self.assertEqual(mask_money('12000000.00'), '12.000.000,00')


    def test_resultado(self):
        """
        Test resultado function
        """
        self.assertTrue(resultado('megasena'))


if __name__ == '__main__':
    unittest.main()
